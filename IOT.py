# Imports
import paho.mqtt.client as mqtt
from sys import exit
import time
import random


# Parametres globaux du script
MQTT_HOST_NAME        = "localhost"         # Hote du courtier
MQTT_HOST_PORT        = 11523              # Port du courtier

MQTT_USERNAME         = "iot"               # Nom d'utilisateur
MQTT_PASSWORD         = "iot"               # Mot de passe
MQTT_CLIENT_ID        = "iot"               # Identifiant du Client

period                =  10                 # la periode en secondes(s) 



# Le "callback" on_connect : gestion de la reponse du courtier (CONNACK)
def on_connect(client, userdata, flags, rc):
    print(" [on_connect]    - Reponse du broker ( CONNACK )   : " + mqtt.connack_string(rc))
    print(" [on_connect]    - Info de la session (existe = 1) : " + str(flags['session present']))
    print(" -- ")



# Le "callback" on_disconnect : reponse du courtier a la deconnexion
def on_disconnect(client, userdata, rc):
    if rc != 0:
        print(" [on_disconnect] - Deconnexion inattendue")
    else:
        print(" [on_disconnect] - Deconnexion du broker")    
    print(" -- ")


# Le "callback" on_subscribe : reponse du courtier a la demande d'abonnement
def on_subscribe(client, userdata, mid, granted_qos):
    print(" [on_subscribe]  - Reponse du broker : mid = " + str(mid) + ", QOS = " + str(granted_qos))
    print(" -- ")


# Le "callback" on_message : message recu du courtier
def on_message(client, userdata, msg):
    print(" [on_message]    - Message recu     : " + str(msg.payload))
    print("                   du sujet (topic) : " + msg.topic)
    print("                   avec une QoS     : " + str(msg.qos))
    print(" -- ")
    if msg.retain == 1:
	    print("                   Message retenu   : oui")
    else:
        print("                   Message retenu   : non")
    
    if msg.topic.strip() == "iot/val" :
        print("                   Modification de la frequence ")
	global period
	int(msg.payload)
        period = int(msg.payload)
        print("                    La Frequence est :"+ str(period) )

    print(" -- ")



#Le "callback" on_publish : reponse du courtier a une demande de publication   
def on_publish(client, userdata, mid):
    print(" [on_publish]    - ID du message publie (mid) : " + str(mid))
    print(" -- ")



# Creation du client MQTT, des "callback" et de l'authentification
client = mqtt.Client(MQTT_CLIENT_ID)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.username_pw_set(username=MQTT_USERNAME, password=MQTT_PASSWORD)
client.on_subscribe  = on_subscribe
client.on_message    = on_message
client.on_publish    = on_publish




# Connexion au courtier
try:
    client.connect(MQTT_HOST_NAME, MQTT_HOST_PORT)
except:
    print(" [connect]       - Erreur sur la connexion")
    print(" -- ")
    exit(0)

#****************************#
#ajouter un timesleep**************
time.sleep(5)


# Abonnement a un sujet
(result, mid) = client.subscribe("iot/val", qos=0)
print(" [subscribe]     - Code erreur de la demande : " + mqtt.error_string(result))
print(" [subscribe]     - ID pour la demande (mid)  : " + str(mid))
print(" -- ")




#creation de message   ######


(rc, mid) = client.publish(topic="iot/temperature", payload="La temperature, la frequence reglable sur le topic iot/val ( par defaut : 10s, sinon: 30s, 1min, 5min, 10min, 30min, 1h", qos=0, retain=True)
print(" [publish]       - Code erreur lors la demande de publication : " + mqtt.error_string(rc))
print(" [publish]       - ID du message a publier (mid)              : " + str(mid))

print(" -- ")



random.seed()
temps = 0
client.loop_start()

while 1:
    #client.reconnect()

        print(period)

        (rc, mid) = client.publish(topic="iot/temperature", payload=random.randrange(0, 50), qos=0)
        print(" [publish]       - Code erreur lors la demande de publication : " + mqtt.error_string(rc))
        print(" [publish]       - ID du message a publier (mid)              : " + str(mid))
        print(" -- ")

	time.sleep(period)

# Deconnexion du broker
client.disconnect()
