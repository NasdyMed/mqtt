@echo off
setlocal enableDelayedExpansion

echo "1 - Copie du fichier mosquitto.conf"
if exist "C:\Program Files\mosquitto\mosquitto.conf" (xcopy "C:\Program Files\mosquitto\mosquitto.conf" mosquittoPersoTmp.conf) else (echo Copie impossible & exit /b)

echo "-----"
echo "2 - Choix du port"
set /p portNum= "Entrez le num du port : "
Rem echo Le num du port est %portNum%

echo "-----"
echo "3 - Authentification"
set /p nClient= "Entrez le nombre de client MQTT : "
Rem echo Le nombre de client est %nClient%

if %nClient% == 0 (
    echo Pas d'authentification & exit /b
)
for /l %%X in (1,1,%nClient%) do (
    set /p name="Entrez le nom du client : "
    Rem echo !name!
    set /p password="Entrez le mot de passe : "
    Rem echo !password!
    if %%X == 1 ("C:\Program Files\mosquitto\mosquitto_passwd.exe" -c -b .\psswd !name! !password!) else ("C:\Program Files\mosquitto\mosquitto_passwd.exe" -b .\psswd !name! !password!)
)
echo Fin de l'authentification

echo "-----"
echo "4 - Fichier ACL"
type nul > acl_file

for /f "tokens=1,* delims=:" %%i in ('findstr /n /r . mosquittoPersoTmp.conf') do if %%i geq 1 if %%i leq 210 echo %%j >> mosquittoPerso.conf
echo port %portNum% >> mosquittoPerso.conf
for /f "tokens=1,* delims=:" %%i in ('findstr /n /r . mosquittoPersoTmp.conf') do if %%i geq 212 if %%i leq 650 echo %%j >> mosquittoPerso.conf
echo allow_anonymous false >> mosquittoPerso.conf
for /f "tokens=1,* delims=:" %%i in ('findstr /n /r . mosquittoPersoTmp.conf') do if %%i geq 652 if %%i leq 668 echo %%j >> mosquittoPerso.conf
echo password_file ./psswd >> mosquittoPerso.conf
for /f "tokens=1,* delims=:" %%i in ('findstr /n /r . mosquittoPersoTmp.conf') do if %%i geq 670 if %%i leq 727 echo %%j >> mosquittoPerso.conf
echo acl_file ./acl_file >> mosquittoPerso.conf
for /f "tokens=1,* delims=:" %%i in ('findstr /n /r . mosquittoPersoTmp.conf') do if %%i geq 729 if %%i leq 988 echo %%j >> mosquittoPerso.conf

del mosquittoPersoTmp.conf
